document.getElementById('inputPrice').onfocus = function () {
    document.getElementById('inputPrice').style.outline = 'none';
    document.getElementById('inputPrice').style.border = '2px solid green';
    newDiv.innerHTML = "";
};
document.getElementById('inputPrice').onblur = function () {
    newDiv = document.createElement('div');
    if ((this.value < 0)||(!this.value)) { 
        document.getElementById('inputPrice').style.border = '2px solid red';
        newDiv.innerHTML = 'Please enter correct price';
        document.body.appendChild(newDiv);
        this.value = " ";
    }
    else {
        document.getElementById('inputPrice').style.border = '0.5px solid grey';
        let newSpan = document.createElement('span');
        newSpan.innerText = `Текущая цена: ${this.value}`;
        let newButton = document.createElement('button');
        newButton.innerText = `X`;
        newButton.style.borderRadius = '50%';
        newButton.style.border = "1px solid grey";
        newButton.style.backgroundColor = 'white';
        document.body.insertBefore(newSpan, document.body.firstChild);
        newSpan.appendChild(newButton);
        newSpan.style.border = "1px solid grey";
        newSpan.style.padding = "5px";
        newSpan.style.borderRadius = "20px";
        newButton.onclick = function () {
            newSpan.remove();
        }
        this.value = "";
    }
};